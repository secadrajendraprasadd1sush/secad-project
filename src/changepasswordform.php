<script defer src="/src/all.js"></script> <!--load all styles -->
<link rel="stylesheet" type="text/css" href="/src/style.css"/>
<?php
require "session_auth.php";
$rand=bin2hex(openssl_random_pseudo_bytes(16));
$_SESSION["nocsrftoken"]=$rand;
?>
<!DOCTYPE html>
<html lang="en">
<h1>Change Password</h1> 
  <h4>By Divya RajendraPrasad</h4>
<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
       <form action="changepassword.php" method="POST" class="form login">
            Username: <?php echo htmlentities($_SESSION["username"]); ?> <br>
            <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
            New Password: <input type="password" class="text_field" name="newpassword" /> <br>
            <button class="button" type="submit">
            Change password 
            </button>
        </form>
</html>


