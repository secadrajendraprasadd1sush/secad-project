<?php
require "database.php";
require "session_auth.php";
$nocsrftoken=$_POST["nocsrftoken"];
if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION["nocsrftoken"])) {

    echo "<script>alert('Cross site request forgery attack is detected!');</script>";
    header("Refresh:0;url=logout.php");
    die();
}

$username = $_SESSION["username"];
$post_id=$_POST["postId"];
$message=$_POST["message"];

if (strlen($message) < 200) {
    if (editPosts($post_id, $username, $message)) {
        http_response_code(204);
    } else {
        http_response_code(500);
    }
} else {
    http_response_code(400);
}
?>