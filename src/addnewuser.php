<?php
require "database.php";


//require "session_auth.php";
//$username=$_SESSION["username"];//$_REQUEST["username"];
//$nocsrftoken=$_POST["nocsrftoken"];
//if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION["nocsrftoken"])) {
//
//    echo "<script>alert('Cross site request forgery attack is detected!');</script>";
//    header("Refresh:0;url=logout.php");
//    die();
//}


$username = sanitize_input($_POST["username"]);
$password = sanitize_input($_POST["password"]);
$name = sanitize_input($_POST["name"]);


if (!validateName($name)) {
    echo "<script>alert('Please enter a valid Name');</script>";
    header("Refresh:0;url=registerationform.php");
    die();
} else if (!validateUsername($username) or !validatePassword($password)) {
    echo "<script>alert('Please enter valid username/password! username=$username;password=$password');</script>";
    header("Refresh:0;url=registerationform.php");
    die();
}


echo "DEBUG:addnewuser.php>username=$username;password=$password;";
if (addnewuser($username, $password, $name, "user", 1)) {
    echo "DEBUG:addnewuser.php>$username is added";
    echo "<script>alert('Your new account is successfully registered,Please Login!');</script>";
    header("Refresh:0;url=registerationform.php");
} else {
    echo "DEBUG:addnewuser.php>$username cannot be added";
    echo "<script>alert('Something was wrong with your registeration');</script>";
    header("Refresh:0;url=registerationform.php");
}
?>
