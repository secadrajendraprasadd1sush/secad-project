<!DOCTYPE html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css"
          href="/src/style.css">
</head>
<body>
<div id="navwrapper">
    <div id="navbar">
        <table class="tablewrapper">
            <tr>
                <td class="row1">Username</td>
                <td class="row1">Password</td>
            </tr>
            <tr>
                <form action="home.php" method="POST">
                    <td><input type="text" class="inputtext" name="username">
                    </td>
                    <td><input type="password" class="inputtext" name="password">
                    </td>
                    <td>
                        <button type="submit" id="button">Log In</button>
                    </td>
                </form>

            </tr>
            <tr>

        </table>

        <h1 class="logowrapper">Mini Facebook</h1>

    </div>
</div>

<div id="contentwrapper">
    <div id="content">

        <div id="leftbod">
            <?php
            //some code here
            echo "Current time: " . date("Y-m-d h:i:sa")
            ?>

            <div class="connect bolder">
                Connect with friends and the
                world around you on Mini Facebook.
            </div>

            <form class="formbox" action="adminlogin.php" method="POST">

                Username:<input id="username" type="text" class="inputbody in2" name="username" required
                                pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$" title="Please enter a valid email as username"
                                placeholder="Your email address" required/>

                <div class="formbox">
                    Password: <input id="admin_password" name="password" type="password" class="inputbody in2"
                                     title="Password must contain: Minimum 8 characters atleast 1 Alphabet and 1 Number"
                                     required/>
                    <br/>
                </div>
                <td>
                    <button type="submit" id="button">Admin Login</button>
                </td>
            </form>
        </div>

        <div id="rightbod">
            <div class="signup bolder">Sign Up</div>
            <div class="free bolder">It's free and always will be</div>
            <form class="formbox" action="addnewuser.php" method="POST">

                Username:<input id="username" type="text" class="inputbody in2" name="username" required
                                pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$" title="Please enter a valid email as username"
                                placeholder="Your email address" required/>
                <div class="formbox">
                    Name:<input id="name" type="text" class="inputbody in2" name="name" required
                                pattern="^([a-zA-Z]{2,}\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\s?([a-zA-Z]{1,})?)"
                                placeholder="Example: Divya Prasad"/> <br>
                </div>

                <div class="formbox">
                    Password: <input id="password" name="password" type="password" class="inputbody in2"
                                     title="Password must contain: Minimum 8 characters atleast 1 Alphabet and 1 Number"
                                     placeholder="Enter Password" required
                                     pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"/>
                    <br/>
                </div>
                <div class="formbox">
                    Confirm Password:<input id="confirm_password" type="password" class="inputbody in2"
                                            name="confirm_password">
                </div>
                <div class="formbox">
                    <p id="msg"></p>
                </div>
                <div class="formbox">
                    <button disabled id="sub_btn" type="submit" class="signbut bolder">Sign Up</button>
                </div>

            </form>
        </div>
    </div>
</div>
</body>
<script src="/src/jquery.js"></script>
</html>
<script type="text/javascript">

    $(document).ready(function () {
        $("#confirm_password, #password").on("keyup", function (e) {
            if ($("#password").val() != "" && $("#confirm_password").val() == $("#password").val()) {
                document.getElementById("msg").innerHTML = "Matching";
                document.getElementById("msg").style.color = 'green';
                document.getElementById('sub_btn').disabled = false;
            } else {
                document.getElementById("msg").innerHTML = "Not Matching";
                document.getElementById("msg").style.color = 'red';
                document.getElementById('sub_btn').disabled = true;
            }
        });
    });

    function checkForm() {
        if (document.getElementById("username").value == "") {
            alert("Error: Username cannot be blank!");
            document.getElementById("username").focus();
            return false;
        }

        if (document.getElementById("password").value != "" && document.getElementById("password").value == document.getElementById("confirm_password").value) {
            if (document.getElementById("password").value.length < 6) {
                alert("Error: Password must contain at least six characters!");
                dfocument.getElementById("password").focus();
                return false;
            }
        } else {
            alert("Please check that you've entered and confirmed your passwords are matching!");
            document.getElementById("password").focus();
            return false;
        }
        return true;
    }
</script>

