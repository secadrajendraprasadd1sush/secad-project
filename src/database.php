<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$mysqli = new mysqli('localhost', 'miniFacebookApp', 'Hemu@1116', 'Facebook');
if ($mysqli->connect_errno) {
    printf("Database Connection failed %s\n", $mysqli->connect_error);
    exit();
}
function changepassword($username, $newpassword)
{
    global $mysqli;
    $prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
    echo "DEBUG>prepared_sql= $prepared_sql\n";//return TRUE;//
    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("ss", $newpassword, $username);
    if (!$stmt->execute()) return FALSE;
    return TRUE;
}

function addnewuser($username, $password, $name, $role, $status)
{
    if (!validateUsername($username)) {
        return FALSE;
    }
    if (!validatePassword($password)) {
        return FALSE;
    }
    if (!validateName($name)) {
        return FALSE;
    }
    global $mysqli;
    $prepared_sql = "INSERT INTO users VALUES(?,password(?),?,?,?);";
    if (!$stmt = $mysqli->prepare($prepared_sql)) {
        echo "Prepared Statement error";
        $error = $mysqli->errno . ' ' . $mysqli->error;
        echo "<script>alert('err is :: $error')</script>";
        return FALSE;
    }
    $stmt->bind_param("ssssi", $username, $password, $name, $role, $status);
    if (!$stmt->execute())
        return FALSE;
    return TRUE;
}

function validateName($name)
{
    $name = sanitize_input($name);
    $pattern = '/^[A-Za-z]+([\ A-Za-z]+)*/';
    if (empty($name) or !preg_match($pattern, $name))
        return FALSE;
    return TRUE;
}

function validateUsername($username)
{
    $username = sanitize_input($username);
    echo "$username";
    $pattern = '/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/';
    if (empty($username) or strlen($username) < 6 or !preg_match($pattern, $username))
        return FALSE;
    return TRUE;
}

function validatePassword($password)
{
    $password = sanitize_input($password);
    echo "$password";
    $pattern = '/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/m';
    if (empty($password) or !preg_match($pattern, $password))
        return FALSE;
    return TRUE;
}


function sanitize_input($input)
{
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    return $input;
}

function sanitize_message($input)
{
    $input = htmlspecialchars($input);
    return $input;
}

function addPosts($username, $message)
{
    if (!validateUsername($username)) {
        return FALSE;
    }
    if ($message == null) {
        return FALSE;
    }
    global $mysqli;
    $prepared_sql = "INSERT INTO posts (created_by,message) VALUES (?,?);";
    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("ss", $username, $message);
    if (!$stmt->execute()) return FALSE;
    return TRUE;

}

function fetchAllPosts()
{
    global $mysqli;
    $prepared_sql = "SELECT * FROM posts ORDER BY timestamp DESC;";
    if (!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement error";
    if (!$stmt->execute()) echo "Execute Error";
    $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    if (!$arr) exit('No rows');
    echo json_encode($arr);
}


function fetchAllCommentsForPost($post_id)
{
    global $mysqli;
    $prepared_sql = "SELECT * FROM comment WHERE post_id=? ORDER BY timestamp ASC;";
    if (!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement errr";
    $stmt->bind_param("s", $post_id);
    if (!$stmt->execute()) echo "Execute Error";
    $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    if (!$arr) echo json_encode($arr);
    echo json_encode($arr);
}

function editPosts($post_id, $username, $message)
{
    if ($message == NULL) {
        return FALSE;
    }

    if ($post_id == null) {
        return FALSE;
    }
    global $mysqli;
    $prepared_sql = "UPDATE posts SET message=? WHERE post_id=? and created_by=?;";
    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("sss", $message, $post_id, $username);
    if (!$stmt->execute()) return FALSE;
    if (!$stmt->affected_rows == 1) return FALSE;
    return TRUE;

}

function deletePosts($post_id, $username)
{

    if ($post_id == null) {
        return FALSE;
    }


    global $mysqli;
    $prepared_sql = "DELETE FROM posts WHERE post_id=? and created_by=?;";
    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("ss", $post_id, $username);
    if (!$stmt->execute()) return FALSE;
    if (!$stmt->affected_rows == 1) return FALSE;
    return TRUE;
}

function addComment($post_id, $username, $message)
{
    if (!validateUsername($username)) {
        return FALSE;
    }
    if ($post_id == null) {
        return FALSE;
    }
    if ($message == null) {
        return FALSE;
    }
    global $mysqli;
    $prepared_sql = "INSERT INTO comment (created_by,message,post_id) VALUES (?,?,?);";
    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("sss", $username, $message, $post_id);
    if (!$stmt->execute()) return FALSE;
    return TRUE;

}

function fetchAllUsers()
{
    global $mysqli;
    $prepared_sql = "SELECT username,status FROM users where role='User'";
    if (!$stmt = $mysqli->prepare($prepared_sql)) echo "Prepared Statement error";
    if (!$stmt->execute()) echo "Execute Error";
    $arr = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
    if (!$arr) exit('No rows');
    echo json_encode($arr);
}

function updateUserStatus($username, $status)
{
    global $mysqli;
    $prepared_sql = "UPDATE users SET status=? WHERE username=?;";
    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    $stmt->bind_param("ss", $status, $username);
    if (!$stmt->execute()) return FALSE;
    if (!$stmt->affected_rows == 1) return FALSE;
    return TRUE;
}