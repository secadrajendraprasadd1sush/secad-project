<?php
$lifetime = 15 * 60;
$path = "/src";
$domain = "192.168.56.101";
$secure = TRUE;
$httponly = TRUE;
session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
session_start();
if (isset($_POST["username"]) and isset($_POST["password"])) {
    if (securechecklogin($_POST["username"], $_POST["password"])) {
        $_SESSION["logged"] = TRUE;
        $_SESSION["username"] = $_POST["username"];
        $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];

    } else {
        echo "<script>alert('Invalid username/password or You are deactivated by admin');</script>";
        echo "invalid pass";
        session_destroy();
        unset($_SESSION["logged"]);
        header("Refresh:0; url=registerationform.php");
        die();
    }
}
$username = $_SESSION["username"];
if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You have not login. Please login first');</script>";
//session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}
if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
    echo "<script>alert('Session hijacking is detected');</script>";
    session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}
?>

<?php
require "session_auth.php";
$rand = bin2hex(openssl_random_pseudo_bytes(16));
$_SESSION["nocsrftoken"] = $rand;
?>

<?php

function securechecklogin($username, $password)
{

    $mysqli = new mysqli('localhost', 'miniFacebookApp', 'Hemu@1116', 'Facebook');
    if ($mysqli->connect_errno) {
        printf("Database Connection failed %s\n", $mysqli->connect_error);
        exit();
    }
    $prepared_sql = "SELECT * FROM users WHERE username= ? " .
        " AND password=password(?) AND status=1;";

//echo "DEBUG>sql=$prepared_sql";//return TRUE;
    if (!$stmt = $mysqli->prepare($prepared_sql))
        echo "Prepared Statement error";
    $stmt->bind_param("ss", $username, $password);
    if (!$stmt->execute()) echo "Execute Error";
    if (!$stmt->store_result()) echo "Store result_error";
    $result = $stmt;
    if ($result->num_rows == 1)
        return TRUE;
    return FALSE;
}

?>


<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8"/>
    <meta name="" content=""/>
    <title>facebook</title>
    <script defer src="/src/all.js"></script> <!--load all styles -->
    <link rel="stylesheet" type="text/css" href="/src/home.style.css"/>
</head>

<body onload="fetchAndRefreshAllPosts()">
<header>
    <a class="first" href="#"><i class="fab  fa-facebook-f"></i></a>
    <a class="second" href="#">
        <i class="fab fa-facebook-messenger" style="cursor:pointer;outline:none;" onclick="startChat();"></i>
    </a>
    <div class="pro-hom" style="left:82%">
        <a href="#">
            <div class="profile">
                <div class="image01"></div>
                <p>divya</p>
            </div>
        </a>
        <a href="changepasswordform.php">
            <div class="home">
                <p>change password</p>
            </div>
        </a>

        <a href="logout.php">
            <div class="logout">
                <p>logout</p>
            </div>
        </a>
    </div>
</header>

<div class="boxContainer">
    <div class="lineSplit"></div>
    <textarea class="messageBox" id="postMessage" placeholder="What's on your mind?" maxlength="200"></textarea>
    </br>
    <input class="postButton" id="submitPost" type="button" onclick="addNewPost()" value="Post"/>
</div>


<div style="text-align: end;position: fixed;left: 70%;top: 80%; background: #FFFFFF;">
    <iframe src = "https://192.168.56.101:4430/chat-client.html" width = "400" height = "150" scrolling="auto">
        Sorry your browser does not support inline frames.
    </iframe>
</div>



<div class="timeline" id="timeline">

</div>
</body>
<script src="/src/jquery.js"></script>
</html>
<script type="text/javascript">
    function editPost(postId) {
        console.log("edit request for post id :: " + postId);
        var message = document.getElementById("post-text-area-" + postId).value;
        var nocsrftoken = '<?php echo $rand; ?>';

        var jsonData = {
            "message": message,
            "postId": postId,
            "nocsrftoken": nocsrftoken

        };

        $.ajax({
            type: "POST",
            async: false,
            url: 'editpost.php',
            data: jsonData,
            success: function (data) {
                fetchAndRefreshAllPosts();
            },
            error: function (err) {
                alert("you cannot edit this post");
                fetchAndRefreshAllPosts();

            }
        });
    }

    function deletePost(postId) {

        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "postId": postId,
            "nocsrftoken": nocsrftoken

        };

        $.ajax({
            type: "POST",
            async: false,
            url: 'deletePosts.php',
            data: jsonData,
            success: function (data) {
                fetchAndRefreshAllPosts();
            },
            error: function (err) {
                alert("you cannot delete this post");
                fetchAndRefreshAllPosts();

            }
        });
    }

    function fetchCommentsForPost(postId) {
        var comments = null;

        $.ajax({
            type: "POST",
            async: false,
            url: 'fetchallcomments.php?postId=' + postId,
            success: function (data) {
                comments = data;
            },
            error: function (err) {
                console.log(err);
            }
        });

        return comments;
    }

    function addComment(postId) {
        var message = document.getElementById("comments-input-" + postId).value;

        if (message.length <= 0 || message.length > 200) {
            alert("comment message is either empty or too big");
            return;
        }
        var userName = '<?php echo $username; ?>';
        var jsonData = {
            "userName": userName,
            "message": message,
            "postId": postId
        };

        $.ajax({
            type: "POST",
            url: 'addcomment.php',
            data: jsonData,
            success: function (data) {
                fetchAndRefreshAllPosts();
            },
            error: function (err) {
                console.log(err);
            }
        });

    }

    function addNewPost() {
        var message = document.getElementById("postMessage").value;
        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "message": message,
            "nocsrftoken": nocsrftoken
        };
        $.ajax({
            type: "POST",
            url: 'addpost.php',
            data: jsonData,
            success: function (data) {
                fetchAndRefreshAllPosts();
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    function fetchAndRefreshAllPosts() {

        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "nocsrftoken": nocsrftoken
        };

        $.ajax({
            type: "POST",
            url: 'fetchallpost.php',
            data:jsonData,
            success: function (data) {
                refreshPosts(data);
            },
            error: function (err) {
                console.log(err);
                return err
            }
        });
    }


    function refreshPosts(posts) {
        $('#timeline').html('');
        var i;
        for (i = 0; i < posts.length; i++) {

            var postCard = '<div class="facebook-card-header" id="' + posts[i].post_id + '">' +
                '<a class="facebook-card-user-name">' + posts[i].created_by + '</a>' +
                '</div>' +
                '<p>' +
                '<a class="facebook-card-content-user">' +
                '</a><textarea disabled class="post-text-area" id="post-text-area-' + posts[i].post_id + '" >'
                + posts[i].message +
                '</textarea></p>';


            var editArea = '<div class="facebook-like-comment-share" id="' + posts[i].post_id + '">' +
                '<div class="facebook-icons">' +
                '<a class="facebook-photo">' +
                '<i class="fas fa-edit"  style="cursor:pointer;outline:none;" onclick="enableEditPost(' + posts[i].post_id + ')" ></i>' +
                '<input onclick="editPost(' + posts[i].post_id + ')" type="button" class="postButton" id="reSubmitPost-' + posts[i].post_id + '" style="display:none; position:relative; bottom:25px; left:5px;" value="Post"/>' +
                '<input onclick="deletePost(' + posts[i].post_id + ')" type="button" class="postButton" id="deletePost-' + posts[i].post_id + '" style="display:none; position:relative; bottom:25px; left:5px;" value="Delete"/>' +
                '</a>' +
                '</div>' +
                '</div>';

            var comments = fetchCommentsForPost(posts[i].post_id);
            var commentsForPosts = '';

            if (comments != null) {
                var j;
                for (j = 0; j < comments.length; j++) {
                    commentsForPosts = commentsForPosts + '<br>' +
                        '                            <div class="facebook-user-comment1" style="height:50px;text-align:left;">' +
                        '                                <p><a class="user-comment" width="32px">' + comments[j].created_by + ' </a> <a class="user-name-tag">' +
                        '                                        ' + comments[j].message + '</a></br>' +
                        '                                </p>' +
                        '                            </div>' +
                        '                            <br>'
                }
            }

            var completePostCard = '<section>' +
                '        <div class="facebook-card">' +
                '            <!--   post area start -->' +
                postCard +
                '            <!--   post area end -->' +
                '' +
                '            <div class="facebook-card-content">' +
                '                <!--   comments area start -->' +
                editArea +
                '                <!--   comments area end -->' +
                '' +
                '                <!--   comments area start -->' +
                '                <div class="comments-content">' +
                '                    <div class="facebook-reactions">' +
                '                        <hr>' +
                '' +
                '                        <div class="facebook-comments" id="comments-area-' + posts[0].post_id + '">' +
                '' + commentsForPosts +
                '                            <!--   add a new comment start  -->' +
                '                            <div>' +
                '                                <input id="comments-input-' + posts[i].post_id + '" class="comments-input" type="text" placeholder=" Write a comment..."/>' +
                '                                <span style="color: #4e69a2;">' +
                '                                    <i class="fa fa-paper-plane fa-2x" aria-hidden="true"  style="cursor:pointer;outline:none;" onclick="addComment(' + posts[i].post_id + ')"></i>' +
                '                                </span>' +
                '                                </br>' +
                '                            </div>' +
                '                            <!--   add a new comment box end  -->' +
                '                        </div>' +
                '' +
                '                    </div>' +
                '                </div>' +
                '                <!--   comments area end -->' +
                '' +
                '            </div>' +
                '        </div>' +
                '    </section>';

            $("#timeline").append(completePostCard);


        }


    }

    function enableEditPost(postId) {
        document.getElementById("post-text-area-" + postId).disabled = false;
        document.getElementById("reSubmitPost-" + postId).style.display = "initial";
        document.getElementById("deletePost-" + postId).style.display = "initial";

    }

    function startChat() {
        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "nocsrftoken": nocsrftoken
        };

        $.ajax({
            type: "POST",
            url: 'chatform.php',
            data:jsonData,
            success: function (data) {
              document.write(data);
            },
            error: function (err) {
                console.log(err);
                return err
            }
        });
    }


</script>

