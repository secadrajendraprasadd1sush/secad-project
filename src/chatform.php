<?php
require "session_auth.php";
$nocsrftoken = $_POST["nocsrftoken"];
if (!isset($nocsrftoken) or ($nocsrftoken != $_SESSION["nocsrftoken"])) {
    echo "<script>alert('Cross site request forgery attack is detected!');</script>";
    header("Refresh:0;url=logout.php");
    die();
}
echo "<script>alert('You will be redirected to chat page!');</script>";
header('Location: chat-client.html');
exit();
?>


