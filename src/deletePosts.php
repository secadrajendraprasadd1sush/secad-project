<?php
require "database.php";
require "session_auth.php";
$nocsrftoken=$_POST["nocsrftoken"];
if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION["nocsrftoken"])) {

    echo "<script>alert('Cross site request forgery attack is detected!');</script>";
    header("Refresh:0;url=logout.php");
    die();
}

$username = $_SESSION["username"];
$post_id=$_POST["postId"];

if (deletePosts($post_id, $username)) {
    http_response_code(204);
}else {
    http_response_code(500);
    }

?>