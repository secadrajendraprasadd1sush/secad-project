<?php
require "database.php";

require "session_auth.php";
$nocsrftoken=$_POST["nocsrftoken"];
if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION["nocsrftoken"])) {

    echo "<script>alert('Cross site request forgery attack is detected!');</script>";
    header("Refresh:0;url=logout.php");
    die();
}

$message = sanitize_message($_POST["message"]);
$userName = $_SESSION["username"];
if (strlen($message) < 200 || validateUsername($userName)) {
    if (addPosts($userName, $message)) {
        http_response_code(204);
    } else {
        http_response_code(500);
    }
} else {
    $data = ['error' => 'Bad Request'];
    header('Content-type: application/json');
    json_encode($data);
    http_response_code(400);
}

?>