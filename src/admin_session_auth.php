<?php
$lifetime = 15 * 60;
$path = "/src";
$domain = "192.168.56.101";
$secure = TRUE;
$httponly = TRUE;
session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
session_start();

if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You have to login first!');</script>";
    session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}

if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {

    echo "<script>alert('Session hijacking attack is detected!');</script>";
    session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}

if (!$_SESSION["isAdminUser"]) {
    echo "<script>alert('You are not authorized only systems are authorized to perform this action');</script>";
    session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}
?>