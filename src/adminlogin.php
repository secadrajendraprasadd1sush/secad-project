<?php
$lifetime = 15 * 60;
$path = "/src";
$domain = "192.168.56.101";
$secure = TRUE;
$httponly = TRUE;
session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
session_start();
if (isset($_POST["username"]) and isset($_POST["password"])) {
    if (securechecklogin($_POST["username"], $_POST["password"])) {

        $_SESSION["logged"] = TRUE;
        $_SESSION["username"] = $_POST["username"];
        $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
        $_SESSION["isAdminUser"] = TRUE;

    } else {
        echo "<script>alert('Invalid username/password');</script>";
        echo "invalid pass";
        session_destroy();
        header("Refresh:0; url=registerationform.php");
        die();
    }
}
$username = $_SESSION["username"];

if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You have not login. Please login first');</script>";
    session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}
if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
    echo "<script>alert('Session hijacking is detected');</script>";
    session_destroy();
    header("Refresh:0; url=registerationform.php");
    die();
}
?>
<?php
require "admin_session_auth.php";
$rand = bin2hex(openssl_random_pseudo_bytes(16));
$_SESSION["nocsrftoken"] = $rand;
?>


<?php

function securechecklogin($username, $password)
{

    echo "<script>alert('username :: '+ $username + 'password :: '+password);</script>";
    $mysqli = new mysqli('localhost', 'miniFacebookApp', 'Hemu@1116', 'Facebook');
    if ($mysqli->connect_errno) {
        printf("Database Connection failed %s\n", $mysqli->connect_error);
        exit();
    }
    $prepared_sql = "SELECT * FROM users WHERE username= ? AND password=password(?) AND role='Admin';";
    if (!$stmt = $mysqli->prepare($prepared_sql))
        echo "Prepared Statement error";
    $stmt->bind_param("ss", $username, $password);
    if (!$stmt->execute()) echo "Execute Error";
    if (!$stmt->store_result()) echo "Store result_error";
    $result = $stmt;
    if ($result->num_rows == 1)
        return TRUE;
    return FALSE;
}

?>

<html>
<head>
    <meta charset="utf-8"/>
    <meta name="" content=""/>
    <title>facebook</title>
    <script defer src="/src/all.js"></script> <!--load all styles -->
    <link rel="stylesheet" type="text/css" href="/src/home.style.css"/>
</head>

<body>
<header>
    <a class="first" href="#"><i class="fab  fa-facebook-f"></i></a>
    <div class="pro-hom" style="left:82%">
        <a href="#">
            <div class="profile">
                <div class="image01"></div>
                <p><?php echo $username; ?></p>
            </div>
        </a>

        <a href="logout.php">
            <div class="logout">
                <p>logout</p>
            </div>
        </a>
    </div>
</header>

<div id="users-table">
    <div class="container">
        <div class="table-responsive table-styles">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">User Name</th>
                    <th scope="col">Active / Blocked</th>
                    <th scope="col">Block/UnBlock</th>
                </tr>
                </thead>
                <tbody id="table-body">

                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
<script src="/src/jquery.js"></script>
</html>

<script type="text/javascript">

    $(document).ready(function () {
        fetchAllUsers()
    });

    function fetchAllUsers() {
        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "nocsrftoken": nocsrftoken
        };

        $.ajax({
            type: "POST",
            url: 'fetchallusers.php',
            data: jsonData,
            success: function (data) {
                populateUsers(data);
            },
            error: function (err) {
                return err
            }
        });
    }

    function populateUsers(users) {
        var i;
        if (users != null) {
            for (i = 0; i < users.length; i++) {
                var status = 'Active';
                var userName = users[i].username;
                var btn = '<input class="postButton" id="statusToggle" type="button" onclick="blockUser(\'' + userName + '\')" value="Block"/>';
                if (users[i].status == 0) {
                    status = "Blocked";
                    btn = '<input class="postButton" id="statusToggle" type="button" onclick="unblockUser(\'' + userName + '\')" value="Unblock"/>';
                }

                $("#table-body").append(' <tr>' +
                    '                    <td class="noBorder">' + userName + '</td>' +
                    '                    <td class="noBorder">' + status + '</td>' +
                    '                    <td class="noBorder">' + btn + '</td>' +
                    '                </tr>');
            }
        }
    }

    function unblockUser(userId) {

        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "status": 1,
            "username": userId,
            "nocsrftoken": nocsrftoken

        };
        $.ajax({
            type: "POST",
            url: 'updateUserStatus.php',
            data: jsonData,
            success: function (data) {
                fetchAllUsers();
            },
            error: function (err) {
                console.log(err);
            }
        });

        //
        console.log("unblock :: " + userId)
    }

    function blockUser(userId) {
        var nocsrftoken = '<?php echo $rand; ?>';
        var jsonData = {
            "status": 0,
            "username": userId,
            "nocsrftoken": nocsrftoken

        };
        $.ajax({
            type: "POST",
            url: 'updateUserStatus.php',
            data: jsonData,
            success: function (data) {
            },
            error: function (err) {
                console.log(err)
            }
        });
        console.log("block :: " + userId)
    }

</script>