   **CPS 475/575 - Secure Application Development**
   
   **Final Project - Mini Facebook Application**
   
Divya Rajendra Prasad-1016520170/rajendraprasadd1@udayton.edu        
Sushmitha Anjanappa-1016507140/anjanappas1@udayton.edu 
            
**Table of Contents:**
                          
                          
S.No    | Title               |
--------|---------------------|
  1     | Introduction        | 
  2     | Design              |   
  3     | Functional Scenarios|
  4     | Security Analysis   |  
  5     | Demo                |  
  6     | Source Code         | 
  7     | References          |  




  **1.Introduction:**
  
  In this assignment, I am learning to build an end to end application (mini Facebook) from the client, server validation to database validation. Also,by doing this assignment, I have gained proficient knowledge in PHP including prevention of SQL injection attacks, Session hijacking, XSS attacks, and cross-site scripting attacks.

  
  **BitBucket Commit Link:**
  
https://bitbucket.org/secadrajendraprasadd1sush/secad-project/commits/
 
  **2.Design:**
     
   **2.1 Database design:**
  
  ![Image of Database Design] (Image/DataBaseDesign.png)

 **2.2 User Interface:**
 The Registration form includes the login for the existing users and signs up form for the new users to register. We leveraged the existing open-source HTML, CSS code snippets from js fiddle and customized them to create a Facebook look alike landing page.

**2.3 Functionalities of Application:**

1. Valid User can register to mini Facebook Application 

2. Registered user can login/logout to application 

3. Logged in user can change the password of their username

4. Superusers will be able to login to the application

5. Superuser and the regular user will be differentiated by attribute “Role”

6. Superusers will be able to activate/deactivate regular users.

7. Logged in users can posts/comment on the home page.

8. User can edit/delete  their own post

9. Alert message will be displayed if they edit/delete other user's post

10.Active user and inactive user(deactivated by admin) will be differentiated by the 'Status' field.

11.Logged in users can chat with other logged in users

12.Session Hijacking,SQL i injection,XSS attack,CSRF attacks are prevented and user is warned with an alert message.



**3. Implementation & security analysis**

Achieved secure and robust programming due to the following reasons,

**1.Input validations:** The inputs are validated at every layer. In the client, server-side validation, we also validate at the database end. Thus preventing attacks or code injection.

**2. Fixing XSS attacks:** By using htmlentities, All the data from users is sanitized.

**3. Preventing SQL injection attacks:** Using prepared statements, to handle the SQL injection in user input fields and handling the message appropriately.

**4. Preventing Session hijacking:** We apply Defense in depth layer to prevent session hijacking. By including session parameters and comparing that with the browser information.

**5. Access control:** Only logged in users will be able to change the password and not logged in users will not be able to change the password. We store the username from session authentication in the change password layout.

**6. Preventing CSRF Attacks:** We generate a secret token and store that in the session. Validating the token from the session with the request. If the token doesn’t match we can identify that as an attack and handling it appropriately.

**7. Robust Programming:** All the above attacks are detected and handled appropriately without abnormal termination.

**4.Demo:**

[Demo Link:](https://recordings.join.me/Keme5K74Lk-iuVapJA-r6A)

**5.Source Code**

* [ Registerationform.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/registerationform.php)
* [Home.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/home.php)
* [fetchallpost.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/fetchallpost.php)
* [fetchallcomments.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/fetchallcomments.php)
* [fetchallpost.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/fetchallpost.php)
* [fetchallusers.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/fetchallusers.php)
* [editpost.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/editpost.php)
* [database.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/database.php)
* [deletepost.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/deletePosts.php)
* [addpost.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/addpost.php)
* [addcomment.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/addcomment.php)
* [addnewuser.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/addnewuser.php)
* [chatform.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/chatform.php)
* [chat-client.html](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/Chat/chat-client.html)
* [Chatserver.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/Chat/socketio-chatserver.js)
* [admin_session_auth.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/admin_session_auth.php)
* [adminhomepage.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/adminhomepage.php)
* [adminlogin.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/adminlogin.php)
* [session_auth.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/session_autht.php)
* [updateUserStatus.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/updateUserStatus.php)
* [all.js](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/all.js)
* [changePasswordform.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/changepasswordform.php)
* [changepassword.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/changepassword.php)
* [logout.php](https://bitbucket.org/secadrajendraprasadd1sush/secad-project/src/master/src/logout.php)

**6.References**

Home page look alike of facebook is achieved by leveraging the existing open-source HTML, CSS code snippets from js fiddle and Codepen.

[Reference of Facebook css:]( https://codepen.io/MustafaEmad/details/NArYZL)

