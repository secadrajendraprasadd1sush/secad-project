-- MySQL dump 10.13  Distrib 5.7.19, for Linux (i686)
--
-- Host: localhost    Database: Facebook
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `comment_id` int(100) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) NOT NULL,
  `post_id` int(100) DEFAULT NULL,
  `message` varchar(200) DEFAULT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment`
--

LOCK TABLES `comment` WRITE;
/*!40000 ALTER TABLE `comment` DISABLE KEYS */;
INSERT INTO `comment` VALUES (1,'divyaprasad16@gmail.com',7,'first to comment yay!!!.','2020-04-28 00:10:12'),(2,'divyaprasad16@gmail.com',6,'commen on the social dist one','2020-04-28 00:14:12'),(3,'divyaprasad16@gmail.com',7,'Hi','2020-04-28 11:42:34'),(4,'rajendraprsadd1@udayton.edu',8,'my comment on demo','2020-04-28 20:40:11'),(5,'rajendraprsadd1@udayton.edu',12,'Checking???','2020-04-30 00:16:29'),(6,'rajendraprsadd1@udayton.edu',12,'It is working???????????????????????????????????/38748y23uihewdjknsa021-`#$%^&amp;*()','2020-04-30 00:16:56'),(7,'rajendraprsadd1@udayton.edu',13,'You are rocking!!!!','2020-04-30 11:16:06'),(8,'rajendraprsadd1@udayton.edu',12,'hi','2020-05-03 00:30:15');
/*!40000 ALTER TABLE `comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `post_id` int(10) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) NOT NULL,
  `message` varchar(200) DEFAULT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'divyaprasad16@gmail.com','my first post!!!','2020-04-27 00:01:08'),(3,'divyaprasad16@gmail.com','my third post.','2020-04-27 22:10:49'),(6,'divyaprasad16@gmail.com','Go Corona','2020-04-27 22:22:48'),(7,'divyaprasad16@gmail.com','lets home this is final!!!','2020-04-27 22:27:47'),(8,'divyaprasad16@gmail.com','Hi this is Demo Test','2020-04-28 13:07:22'),(9,'rajendraprsadd1@udayton.edu','this is my first post','2020-04-28 20:39:55'),(10,'rajendraprsadd1@udayton.edu','Happy birthday @divya','2020-04-28 20:48:44'),(11,'divyaprasad16@gmail.com','csrf post test','2020-04-29 13:20:32'),(12,'divyaprasad16@gmail.com','check this','2020-04-29 13:32:54');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `role` varchar(15) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('Admin','*2D4B3E02ECBFDD0E53D7A52FEBF92AA053465CAD','Divya','Admin',1),('Admin1@gmail.com','*2D4B3E02ECBFDD0E53D7A52FEBF92AA053465CAD','Divya','Admin',1),('divyaprasad16@gmail.com','*909311A55C5F3B00315C4F65F0B6B6ADBFA2FBB5','Divya Prasad','user',0),('divyaprasad1@gmail.com','*909311A55C5F3B00315C4F65F0B6B6ADBFA2FBB5','Divya prasad','user',0),('divyaprasad25@gmail.com','*909311A55C5F3B00315C4F65F0B6B6ADBFA2FBB5','Divya Prasad','user',0),('hemanthkumar@gmail.com','*909311A55C5F3B00315C4F65F0B6B6ADBFA2FBB5','Hemu Vemula','user',1),('hkv9@njit.edi','*909311A55C5F3B00315C4F65F0B6B6ADBFA2FBB5','secad hmu','user',1),('jhfjhjfdhjh2@gmail.com','*0049EC3150138DEF51D3FEB54187CD3C3C26DB67','jfhjhfg kkgfhkgh','user',1),('rajendraprsadd1@udayton.edu','*909311A55C5F3B00315C4F65F0B6B6ADBFA2FBB5','Divya RajendraPrasad','user',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-03 22:56:03
